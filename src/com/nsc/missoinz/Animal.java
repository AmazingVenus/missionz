package com.nsc.missoinz;

import com.google.android.gms.maps.model.LatLng;

public class Animal {
	private int health;
	private int exp;
	private LatLng position;
	
	public Animal(int hp,int stm,int exp,LatLng p){
		this.health = hp;
		this.exp = exp;
		this.position = p;
	}
	
	public void setPlayerHealth(int hp){
		health = hp;
	}
	public int getPlayerHealth(){
		return health;
	}
	
	public void setPlayerExp(int exp){
		this.exp = exp;
	}
	public int getPlayerExp(){
		return exp;
	}
}
