package com.nsc.missoinz;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.cengalabs.flatui.views.FlatEditText;
import com.cengalabs.flatui.views.FlatTextView;
import com.nsc.missoinz.R;

public class EnterName extends Activity {

	SharedPreferences sp;
	SharedPreferences.Editor editor;

	private Button ok_btn;
	private TextView txt;
	private FlatEditText name;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.insert_name);

		sp = getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);
		editor = sp.edit();

		ok_btn = (Button) findViewById(R.id.ok_btn);
		txt = (TextView) findViewById(R.id.textView1);
		name = (FlatEditText) findViewById(R.id.flatEditText1);

		Typeface font = Typeface.createFromAsset(getAssets(),
				"fonts/Arabica.ttf");
		txt.setTypeface(font);
		ok_btn.setTypeface(font);
		name.setTypeface(font);
		ok_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(),
						ModeSelection.class);
				if (sp.getBoolean("IS_FIRST_RUN", true) == true) {
					editor.putBoolean("IS_FIRST_RUN", false);
					editor.putString("PLAYER_NAME", name.getText().toString());
					editor.putInt("PLAYER_MAX_HP", 500);
					editor.putInt("PLAYER_CUR_HP", 500);
					editor.putInt("PLAYER_MAX_EXP", 100);
					editor.putInt("PLAYER_CUR_EXP", 0);
					editor.putInt("PLAYER_MAX_STA", 200);
					editor.putInt("PLAYER_CUR_STA", 200);
					editor.putInt("PLAYER_WALKED_TOTAL", 0);
					editor.commit();
				}
				startActivity(i);
			}
		});
	}
}
