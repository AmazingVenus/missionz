package com.nsc.missoinz;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.cengalabs.flatui.views.FlatTextView;

@SuppressLint("NewApi")
@SuppressWarnings("deprecation")
public class GamePlay extends Activity implements SurfaceHolder.Callback {
	
	Camera mCamera;
    SurfaceView mPreview;
    SensorManager sensorManager;
	Sensor sensor;
	FlatTextView x_axis;
	FlatTextView y_axis;
	FlatTextView z_axis;
	FlatTextView sc;
	ImageView animal;
	ui ss;
	double fx,fy;
	double PI = Math.PI;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN 
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.ingame);
        
        mPreview = (SurfaceView)findViewById(R.id.surfaceView1);
        mPreview.getHolder().addCallback(this);
        mPreview.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        
        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
		sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		
		x_axis = (FlatTextView)findViewById(R.id.flatTextView1);
		y_axis = (FlatTextView)findViewById(R.id.flatTextView2);
		z_axis = (FlatTextView)findViewById(R.id.flatTextView3);
		sc = (FlatTextView)findViewById(R.id.flatTextView4);
		animal = (ImageView)findViewById(R.id.imageView1);
		ss = (ui)findViewById(R.id.view1);
		fx=0;
		fy=0;
		OnClickListener l = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				double rad_x = Math.random()*PI*2;
				double rad_y = Math.random()*PI;
				double sign = Math.random();
				
				if(sign > 0.5)
					rad_y *= -1;
				ss.rx = 10*Math.cos(rad_x)*Math.sin(rad_y);
				ss.ry = 10*Math.sin(rad_x)*Math.sin(rad_y);
				ss.rz = 10*Math.cos(rad_y);
			}
		};
		ss.setOnClickListener(l);
		
    }
    
    public void onResume() {
        Log.d("System","onResume");
        super.onResume();
        mCamera = Camera.open();
        sensorManager.registerListener(AccListener, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }
    
    public void onPause() {
        Log.d("System","onPause");
        super.onPause();
        mCamera.release();
    }
    
    public void surfaceChanged(SurfaceHolder arg0
            , int arg1, int arg2, int arg3) {
        Log.d("CameraSystem","surfaceChanged");
        Camera.Parameters params = mCamera.getParameters();
        List<Camera.Size> previewSize = params.getSupportedPreviewSizes();
        List<Camera.Size> pictureSize = params.getSupportedPictureSizes();
        params.setPictureSize(pictureSize.get(0).width,pictureSize.get(0).height);
        params.setPreviewSize(previewSize.get(0).width,previewSize.get(0).height);
        params.setJpegQuality(100);
        mCamera.setParameters(params);
        mCamera.setDisplayOrientation(90);
        
        try {
            mCamera.setPreviewDisplay(mPreview.getHolder());
            mCamera.startPreview();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void surfaceCreated(SurfaceHolder arg0) {
        Log.d("CameraSystem","surfaceCreated");
        try {
            mCamera.setPreviewDisplay(mPreview.getHolder());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void surfaceDestroyed(SurfaceHolder arg0) { }
    
    SensorEventListener AccListener = new SensorEventListener() {
		double x,y;
		float degree;
		
		@Override
		public void onSensorChanged(SensorEvent event) {
			// TODO Auto-generated method stub
//			
//			x_axis.setText("X : "+event.values[0]);
//			y_axis.setText("Y : "+event.values[1]);
//			z_axis.setText("Z : "+event.values[2]);
//			ui.x = event.values[0];
//			ui.y = event.values[1];
//			ui.z = event.values[2];
//			x = event.values[0];
//			y = event.values[1];
//			//degree = (int) Math.toDegrees(Math.atan(y/x));
//			
//			fx -= event.values[0]*2;
//			fy += event.values[1]*2;
//			
//			ui.fx = fx;
//			ui.fy = fy;
//			if(fx > 200){
//				fx = 200;
//			}
//			if(fy > 200){
//				fy = 200;
//			}
//			if(fx < 0){
//				fx = 0;
//			}
//			if(fy < 0){
//				fy = 0;
//			}
//			
//			degree = (int) Math.toDegrees(Math.atan(fy/fx));
			//animal.setRotation(degree-90);
			int ot = getResources().getConfiguration().orientation;
			switch(ot)
			        {

			        case  Configuration.ORIENTATION_LANDSCAPE:

			            Log.d("my orient" ,"ORIENTATION_LANDSCAPE");
			        break;
			        case Configuration.ORIENTATION_PORTRAIT:
			            Log.d("my orient" ,"ORIENTATION_PORTRAIT");
			            break;

			        case Configuration.ORIENTATION_SQUARE:
			            Log.d("my orient" ,"ORIENTATION_SQUARE");
			            break;
			        case Configuration.ORIENTATION_UNDEFINED:
			            Log.d("my orient" ,"ORIENTATION_UNDEFINED");
			            break;
			            default:
			            Log.d("my orient", "default val");
			            break;
			        }
			//sc.setText("Degree : "+ degree);
		}
		
		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			// TODO Auto-generated method stub
			
		}
	};
	
}
