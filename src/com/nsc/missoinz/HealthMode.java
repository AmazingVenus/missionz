package com.nsc.missoinz;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.cengalabs.flatui.views.FlatEditText;

public class HealthMode extends Activity {
	TextView t1;
	TextView t2;
	TextView t3;
	FlatEditText cal;
	Button ok_btn;
	SharedPreferences sp;
	SharedPreferences.Editor editor;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.health_mode);
		t1 = (TextView) findViewById(R.id.textView1);
		t2 = (TextView) findViewById(R.id.textView2);
		t3 = (TextView) findViewById(R.id.textView3);
		ok_btn = (Button)findViewById(R.id.button1);
		cal = (FlatEditText) findViewById(R.id.flatEditText1);
		Typeface font = Typeface.createFromAsset(getAssets(),
				"fonts/Arabica.ttf");
		t1.setTypeface(font);
		t2.setTypeface(font);
		t3.setTypeface(font);
		cal.setTypeface(font);
		
        sp = getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);
        editor = sp.edit();
		
		ok_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(), Main.class);
				editor.putInt("DAILY_CAL", Integer.parseInt(cal.getText().toString()));
				editor.commit();
				startActivity(i);
			}
		});
	}

}
