package com.nsc.missoinz;

import com.cengalabs.flatui.views.FlatButton;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends Activity {
	
	private Button open_map;
	private FlatButton reset;
	SharedPreferences sp;
	SharedPreferences.Editor editor;
    
    double lat, lng;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        sp = getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);
        editor = sp.edit();
        

        
        open_map = (Button)findViewById(R.id.button2);
        open_map.setOnClickListener(new OnClickListener() {
        
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		        if(sp.getBoolean("IS_FIRST_RUN", true) == true){
		        	Intent i = new Intent(getApplicationContext(), EnterName.class);
					startActivity(i);
		        }else{
		        	Intent i = new Intent(getApplicationContext(), ModeSelection.class);
					startActivity(i);
		        }
			}
		});
        
        reset = (FlatButton)findViewById(R.id.flatButton1);
        reset.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				editor.putBoolean("IS_FIRST_RUN", true);
				editor.commit();
			}
		});
    }
}
