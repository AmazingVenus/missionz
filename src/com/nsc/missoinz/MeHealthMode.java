package com.nsc.missoinz;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MeHealthMode extends Activity{
	
	SharedPreferences sp;
	SharedPreferences.Editor editor;
	
    TextView cal_goal_txt;
    TextView cal_burned_txt;
    TextView dist_goal_txt;
    TextView dist_walked_txt;
    TextView animal_goal_txt;
    TextView animal_catch_txt;
    TextView score_earned_txt;
    TextView exp_earned_txt;
    TextView title_txt;
    TextView level_info_txt;
    RoundCornerProgressBar cal_bar;
    RoundCornerProgressBar dist_bar;
    RoundCornerProgressBar animal_bar;
    
    int cal_burned = 111;
    int dist_walked = 60;
    int animal_catch = 10;
    int score = 123;
    int exp = 145;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		
        super.onCreate(savedInstanceState);
        setContentView(R.layout.me_health);
        
        sp = getSharedPreferences("PREF_NAME", Context.MODE_PRIVATE);
        
        
        title_txt = (TextView)findViewById(R.id.textView1);
        level_info_txt = (TextView)findViewById(R.id.textView2);
        cal_goal_txt = (TextView)findViewById(R.id.textView3);
        cal_burned_txt = (TextView)findViewById(R.id.textView4);
        dist_goal_txt = (TextView)findViewById(R.id.textView5);
        dist_walked_txt = (TextView)findViewById(R.id.textView6);
        animal_goal_txt = (TextView)findViewById(R.id.textView7);
        animal_catch_txt = (TextView)findViewById(R.id.textView8);
        score_earned_txt = (TextView)findViewById(R.id.textView9);
        exp_earned_txt = (TextView)findViewById(R.id.textView10);
        
        cal_bar = (RoundCornerProgressBar)findViewById(R.id.roundCornerProgressBar1);
        cal_bar.setMax(sp.getInt("DAILY_CAL", 20));
        cal_bar.setProgress(cal_burned);
        cal_bar.setProgressColor(selectColor(cal_burned, sp.getInt("DAILY_CAL", 20)));
        
        dist_bar = (RoundCornerProgressBar)findViewById(R.id.roundCornerProgressBar2);
        dist_bar.setMax(500);
        dist_bar.setProgress(dist_walked);
        dist_bar.setProgressColor(selectColor(dist_walked, 500));
        
        animal_bar = (RoundCornerProgressBar)findViewById(R.id.roundCornerProgressBar3);
        animal_bar.setMax(sp.getInt("DAILY_ANIMAL", 20));
        animal_bar.setProgress(animal_catch);
        animal_bar.setProgressColor(selectColor(animal_catch, sp.getInt("DAILY_ANIMAL", 20)));
        
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/Arabica.ttf");
        level_info_txt.setTypeface(font);
        cal_goal_txt.setTypeface(font);
        cal_burned_txt.setTypeface(font);
        dist_goal_txt.setTypeface(font);
        dist_walked_txt.setTypeface(font);
        animal_catch_txt.setTypeface(font);
        animal_goal_txt.setTypeface(font);
        score_earned_txt.setTypeface(font);
        exp_earned_txt.setTypeface(font);
        title_txt.setTypeface(font);
        
        title_txt.setTextSize(50);
        level_info_txt.setTextSize(40);
        cal_goal_txt.setTextSize(20);
        cal_burned_txt.setTextSize(20);
        dist_goal_txt.setTextSize(20);
        dist_walked_txt.setTextSize(20);
        animal_catch_txt.setTextSize(20);
        animal_goal_txt.setTextSize(20);
        score_earned_txt.setTextSize(20);
        exp_earned_txt.setTextSize(20);
        
        level_info_txt.setText("Level 1 : " + sp.getString("PLAYER_NAME", "Unkhow"));
        cal_goal_txt.setText("�������㹡���Ҽ�ҭ������ \t\t" + sp.getInt("DAILY_CAL", 0)+" ������.");
        cal_burned_txt.setText("���������Ҽ�ҭ��\t\t\t\t\t\t\t\t\t\t" + cal_burned+" ������");
        
        dist_goal_txt.setText("����������зҧ㹡���Թ�ҧ\t\t" + 500 + " ����.");
        dist_walked_txt.setText("���зҧ����Թ�����\t\t\t\t\t\t\t\t\t\t" + dist_walked + " ����.");
        
        animal_goal_txt.setText("������¨ӹǹ�ѵ�����ͧ�Ѻ\t\t\t\t" + sp.getInt("DAILY_ANIMAL", 10) + " ���");
        animal_catch_txt.setText("�ӹǹ�ѵ����Ѻ������\t\t\t\t\t\t\t\t\t" + animal_catch + " ���");
        
        score_earned_txt.setText("��ṹ������Ѻ\t\t\t\t\t\t\t"+score+" ��ṹ");
        exp_earned_txt.setText("��һ��ʺ��ó������Ѻ\t\t"+exp+" Exp.");
        
	}
	
	private int selectColor(int progress,int max){
		int percent = (int) ((progress/(max*1.0))*100.0);
		Log.d("Percent",""+percent);
		if(percent > 70){
			return Color.rgb(181, 230, 29);
		}
		if(percent > 40){
			return Color.rgb(255, 201, 14);
		}else{
			return Color.rgb(237, 28, 36);
		}
	}
	
}
