package com.nsc.missoinz;

import com.google.android.gms.maps.model.LatLng;

public class Player {
	private int health;
	private int stamina;
	private int exp;
	private LatLng position;
	
	public Player(int hp,int stm,int exp,LatLng p){
		health = hp;
		stamina = stm;
		this.exp = exp;
		position = p;
	}
	
	public void setPlayerHealth(int hp){
		health = hp;
	}
	public int getPlayerHealth(){
		return health;
	}
	
	public void setPlayerStamina(int stamina){
		this.stamina = stamina;
	}
	public int getPlayerStamina(){
		return stamina;
	}
	
	public void setPlayerExp(int exp){
		this.exp = exp;
	}
	public int getPlayerExp(){
		return exp;
	}
}

