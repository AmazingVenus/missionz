package com.nsc.missoinz;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View.OnClickListener;
import android.view.View;
import android.app.Activity.*;
import com.nsc.missoinz.R;
public class ui extends View{

	Paint p;
	int w,h,r;
	public static double x,y,z;
	int count_x,count_y,count_z,count_xx,count_yy,count_zz;
	public static int score;
	boolean yes;
	public static double rx,ry,rz;
	public static double fx,fy;
	public ui(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		p = new Paint();
		r = 200;
		score = 0;
		yes = false;
		count_x = count_y = count_z = count_xx = count_yy = count_zz = 0;
		rx = Math.random()*10;
		ry = Math.random()*10;
		rz = Math.random()*10;
	}
	
	public ui(Context context,AttributeSet attrs) {
		// TODO Auto-generated constructor stub
		super(context, attrs);
		p = new Paint();
		r = 200;
		score = 0;
		yes = false;
		count_x = count_y = count_z = count_xx = count_yy = count_zz = 0;
		rx = Math.random()*10;
		ry = Math.random()*10;
		rz = Math.random()*10;
		
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		w = canvas.getWidth();
		h = canvas.getHeight();
		p.setColor(Color.GREEN);
		//canvas.drawText("rx = "+rx, 0, 500, p);
		//canvas.drawText("ry = "+ry, 0, 600, p);
		//canvas.drawText("rz = "+rz, 0, 700, p);
		p.setColor(Color.argb(60, 0, 121, 255));
		
		int dx = (int) Math.abs(rx-x);
		int dy = (int) Math.abs(ry-y);
		int dz = (int) Math.abs(rz-z);
		
		if(dx < 5 && dy < 5 && dz < 5){
			r--;
		}else{
			r++;
		}
		p.setStrokeWidth(3);
		if(r < 100 && r >= 50){
			p.setColor(Color.argb(60, 240, 160, 13));
		
		}else if(r < 50){
			p.setColor(Color.argb(60, 255, 0, 0));
		}
		if(r < 30){
			yes = true;
			r = 30;
		}else{
			yes = false;
		}
		if(r > 200){
			r = 200;
		}
	    canvas.drawCircle(w/2, h/2, r, p);
	    //canvas.save();
	    //float degree = (float) Math.asin(y) * 180;
	    //p.setTextSize(20);
	    //canvas.drawText("degree = "+degree, 0, 700, p);
	    //canvas.rotate(90,w/2,h/2);
	    //p.setColor(Color.alpha(20));
	    p.setColor(Color.argb(100, 255, 0, 0));
	    p.setStrokeWidth(5);
	    canvas.drawCircle((int)fx, (int)fy, 20, p);
	    //canvas.drawBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.square_animal_9_bear_128px), w/2+64, h/2-74, p);
	    
	    Log.d(this.getClass().getName(), "On Draw Called");
	    invalidate();
	}

}
